const FPIN_RETRY_ATTEMPTS = 5;
const FPIN_RETRY_WAIT_MS = 2000;

function fpin_getState (entity, pins) {
	if (!entity || !entity.data) {
		return;
	}

	let id = entity.data._id;
	let owner = entity.actor;

	const key = owner == null ? id : `${owner.id}.${id}`;
	let state = pins[key];

	if (!state) {
		state = {
			pinned: false,
			entity: entity.collection.entity
		};
		pins[key] = state;
	}

	return state;
}

Hooks.on('ready', () => {
	game.settings.register('foundry-pin', 'pins', {
		name: 'pins',
		default: {},
		scope: 'client'
	});

	const pins = game.settings.get('foundry-pin', 'pins');
	Object.entries(pins).filter(([_, state]) => state.pinned).forEach(([key, state]) => {
		let id = key;
		let owner = null;

		if (id.includes('.')) {
			const split = id.split('.');
			owner = split[0];
			id = split[1];
		}

		let entity;
		if (owner == null) {
			entity = game[CONFIG[state.entity].collection.name.toLowerCase()].get(id);
		} else {
			const actor = game.actors.get(owner);
			if (actor) {
				entity = actor.getEmbeddedEntity('OwnedItem', id);
			}
		}

		if (entity && entity.sheet) {
			let retries = 0;
			const openSheet = async () => {
				try {
					let x = state.x;
					let y = state.y;
					if (state.skycons !== undefined && state.skycons.maxpos !== undefined) {
						x = state.skycons.maxpos.x;
						y = state.skycons.maxpos.y;
					}
					await entity.sheet._render(true, {'left':x, 'right':y});
					entity.sheet.minimize();
					entity.sheet.position.width = state.w;
					entity.sheet.position.height = state.h;
					x = state.x;
					y = state.y;
					if (state.skycons !== undefined && state.skycons.minpos !== undefined) {
						x = state.skycons.minpos.x;
						y = state.skycons.minpos.y;
					}
					entity.sheet.setPosition({top: y, left: x});
					if (state.skycons !== undefined)
						entity.sheet._skycons = JSON.parse(JSON.stringify(state.skycons));
				} catch {
					// There are some race conditions around how other mods
					// load here. I think the best we can do is just retry a
					// few times and hope things are loaded by then.
					retries++;
					entity.sheet._state = Application.RENDER_STATES.NONE;

					if (retries < FPIN_RETRY_ATTEMPTS) {
						setTimeout(openSheet, FPIN_RETRY_WAIT_MS);
					}
				}
			};

			openSheet();
		}
	});
});

Application.prototype._renderOuter = (function () {
	const cached = Application.prototype._renderOuter;
	return async function () {
		const html = await cached.apply(this, arguments);
		if (this.options.pinnable === false) {
			return html;
		}

		const pins = game.settings.get('foundry-pin', 'pins');
		const state = fpin_getState(this.entity, pins);

		if (!state) {
			return html;
		}

		const pin = $('<a class="pin"><i class="fas fa-thumbtack"></i></a>');
		pin.insertBefore(html.find('a.close'));

		if (state.pinned) {
			pin.css('color', 'red');
		}

		pin.click(() => {
			state.pinned = !state.pinned;
			state.x = this.position.left;
			state.y = this.position.top;
			state.h = this.position.height;
			state.w = this.position.width;
			if (this._skycons !== undefined) {
				state.skycons = {};
				if (this._minimized) {
					if (this._skycons.maxpos !== undefined)
						state.skycons.maxpos = {'x':this._skycons.maxpos.x, 'y':this._skycons.maxpos.y};
					state.skycons.minpos = {'x':this.position.left, 'y':this.position.top};
				}
				else {
					if (this._skycons.minpos !== undefined)
						state.skycons.minpos = {'x':this._skycons.minpos.x, 'y':this._skycons.minpos.y};
					state.skycons.maxpos = {'x':this.position.left, 'y':this.position.top};
				}
			}
			game.settings.set('foundry-pin', 'pins', pins);

			if (state.pinned) {
				pin.css('color', 'red');
			} else {
				pin.css('color', pin.next().css('color'));
			}
		});

		return html;
	};
})();

Application.prototype.minimize = (function () {
	return function () {
		if (!this.popOut || [true, null].includes(this._minimized) || !this.element.length) {
			return;
		}

		const window = this.element;
		const header = window.find('.window-header');
		header.children().not('.window-title').not('.close').not('.pin').hide();

		window.css({
			minWidth: 100,
			minHeight: 30,
			height: `${header[0].offsetHeight + 1}px`,
			width: MIN_WINDOW_WIDTH
		}).addClass('minimized');

		this._minimized = true;
	};
})();

Application.prototype._onResize = (function () {
	const cached = Application.prototype._onResize;
	return function () {
		cached.apply(this, arguments);
		const pins = game.settings.get('foundry-pin', 'pins');
		const state = fpin_getState(this.entity, pins);

		if (!state) {
			return;
		}

		state.w = this.position.width;
		state.h = this.position.height;
		game.settings.set('foundry-pin', 'pins', pins);
	};
})();

Draggable.prototype._onDragMouseUp = (function () {
	const cached = Draggable.prototype._onDragMouseUp;
	return function () {
		cached.apply(this, arguments);
		if (!this.app || this.app.options.pinnable === false) {
			return;
		}

		const pins = game.settings.get('foundry-pin', 'pins');
		const state = fpin_getState(this.app.entity, pins);
		if (!state) {
			return;
		}

		state.x = this.app.position.left;
		state.y = this.app.position.top;
		state.h = this.app.position.height;
		state.w = this.app.position.width;
		if (this.app._skycons !== undefined) {
			state.skycons = {};
			if (this.app._minimized) {
				if (this.app._skycons.maxpos !== undefined)
					state.skycons.maxpos = {'x':this.app._skycons.maxpos.x, 'y':this.app._skycons.maxpos.y};
				state.skycons.minpos = {'x':this.app.position.left, 'y':this.app.position.top};
			}
			else {
				if (this.app._skycons.minpos !== undefined)
					state.skycons.minpos = {'x':this.app._skycons.minpos.x, 'y':this.app._skycons.minpos.y};
				state.skycons.maxpos = {'x':this.app.position.left, 'y':this.app.position.top};
			}
		}
		game.settings.set('foundry-pin', 'pins', pins);
	};
})();
